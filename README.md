# Local and landscape drivers of aquatic-to-terrestrial subsidies in riparian ecosystems: a worldwide analysis

This dataset and code correspond to the manuscript: "Local and landscape drivers of aquatic-to-terrestrial subsidies 
in riparian ecosystems: a worldwide analysis"

Authors: Lafage D., Bergman E., Eckstein L., Österling M., Saddler J. and Piccolo J.

## Aim

These dataset and code correpond to a research project:
- Meta-analysis of terrestrial predators' diet in riparian ecosystems (% of aquatic subsidies (preys))
- Landscape and local drivers of aquatic subsidies use by terrestrial predators

## Quick description

- DataCsv folder contains:
    - isotope values for d13C and d15N extracted from 21 studies focusing on the diet of terrestrial
    predators consuming aquatic prey (macro-invertebrates).
    - Landscape (catchment) and local (100m buffer) scale variables

- 1.DataImport.Rmd contains the code to import and tidy the landscape and local variables + diet values computed with 2.DietPartioning.Rmd

- 2.DietPartioning.Rmd contains the code to estimatate % of aquatic/terrestrial subsidies in predators diet. For studies where raw values 
were available for consumers it uses simmr, otherwise it use a modified JAGS code (written in collaboration with A. Parnel) that includes 
standard error as a prior

- 3.MetaAnalysis.Rmd contains to code to run a meta-analysis to estimate the overall contribution of aquatic subsidies to predators'diet. 
The tested effect size is the difference with a balanced diet (50%)

- 4.PartialLeastSquare.Rmd contains the code to run partial least square regressions on spiders and carabid beetles to identify 
variables explaining % of aquatic prey un their diet.

- Figures.Rmd contains the code used for the figures of the manuscript. Figures are then edited using Inkscape

- TableMS.Rmd contains the code used to produce the table in the manuscript.
