---
title: "Drivers of diet"
output: html_notebook
---

# Load libraries
```{r}
library(tidyverse)
library(mixOmics)
library(dummies)

```


# Partial least square regressions

## Prepare spider dataset

```{r}
DataPlsSpid <- Diets %>% 
  dplyr::select(SampleCode, Species, Grp, Guild, DietSimmr,Nb, DietJags) %>%
  # Remove lake likes sites (because of beaver dams)
  filter(!SampleCode %in% c("CAF16S1","CAF16S3","TER17S1", "BRI05S1")) %>% 
  filter(Grp == "Spiders") %>% 
  mutate(DietMix = case_when(Nb == 1 & is.na(DietJags) ~ DietSimmr, # use modified Jags values when no row data for consummers
                              Nb == 1 & !is.na(DietJags)~ DietJags,
                              TRUE ~ DietSimmr)) %>% 
  group_by(SampleCode) %>% 
  summarise(MeanDiet = mean(DietMix)) %>% 
  #join explicative variables
  left_join(OsGrp, by = "SampleCode") %>%
  left_join(OpenCano[,c("SampleCode","MeanOpenCano")],by = "SampleCode") %>%
  left_join(DivVar,by = "SampleCode") %>%
  left_join(VarB100, by = "SampleCode")%>%
  left_join(Coord, by = "SampleCode") %>% 
  left_join(HumanPop[,c(1,3)], by = "SampleCode")%>%
  left_join(Meandering, by = "SampleCode") %>% 
  left_join(Perim2Area, by = "SampleCode") %>% 
  filter(!is.na(Width)) %>% 
  filter(!is.na(Perim2Area)) %>% # no catchment
  left_join(SamplingYear, "StudyCode") %>% 
  dplyr::select(MeanOpenCano, Width, MeanPop, os_18,os_20, os_21, os_22, os_23, OsClassB100, PopB100, OpenCanoB100, Perim2Area, Meandering, FeowId,SamplingYear, MeanDiet)

# MixOmics doesn't want categorical varibles in a dataframe -> convert into dummies
Osb100Spid<-dummies::dummy(DataPlsSpid$OsClassB100, sep = "_")
FeowidSpid<-dummies::dummy(DataPlsSpid$FeowId, sep = "_")
DataPlsSpid<-as.data.frame(cbind(DataPlsSpid, Osb100Spid, FeowidSpid))
```

## Run PLS on spider dataset

```{r}
PlsSpid<-mixOmics::pls(DataPlsSpid[,c(1:8,10:13,15,17:31)], DataPlsSpid[,16], mode = "regression", scale = TRUE, ncomp = 10)

# Choose number of components
TunePlsSpid <- perf(PlsSpid, validation = "Mfold", folds = 10, progressBar = FALSE, nrepeat = 10)  
TunePlsSpid$Q2.total
plot(TunePlsSpid$Q2.total) #-> 2 components

# Run PLS with 2 components
PlsSpid2C<-mixOmics::pls(DataPlsSpid[,c(1:8,10:13,15,17:31)],DataPlsSpid[,16], mode = "regression", scale = TRUE, ncomp = 2)

TunePlsSpid2C <- perf(PlsSpid2C, validation = "Mfold", folds = 10, progressBar = FALSE, nrepeat = 10)   
TunePlsSpid2C$R2
TunePlsSpid2C$loadings
TunePlsSpid2C$mat.c

# Select explicative variables according to vip values

PlsSpid2C_Vip<-vip(PlsSpid2C)
PlsSpid2C_Vip

# Plot loadings values
plotLoadings(PlsSpid2C, block = 1, comp = , xlim = c(-0.3,0.3), border = TRUE)

```

## Prepare carabid dataset

```{r}
DataPlsCarab <- Diets %>% 
  dplyr::select(SampleCode, Species, Grp, Guild, DietSimmr,Nb, DietJags) %>%
  # Remove lake likes sites (because of beaver dams)
  filter(!SampleCode %in% c("CAF16S1","CAF16S3","TER17S1", "BRI05S1")) %>% 
  filter(Grp == "Beetles") %>% 
  mutate(DietMix = case_when(Nb == 1 & is.na(DietJags) ~ DietSimmr, # use modified Jags values when no row data for consummers
                              Nb == 1 & !is.na(DietJags)~ DietJags,
                              TRUE ~ DietSimmr)) %>% 
  group_by(SampleCode) %>% 
  summarise(MeanDiet = mean(DietMix)) %>% 
  #join explicative variables
  left_join(OsGrp, by = "SampleCode") %>%
  left_join(OpenCano[,c("SampleCode","MeanOpenCano")],by = "SampleCode") %>%
  left_join(DivVar,by = "SampleCode") %>%
  left_join(VarB100, by = "SampleCode")%>%
  left_join(Coord, by = "SampleCode") %>% 
  left_join(HumanPop[,c(1,3)], by = "SampleCode")%>%
  left_join(Meandering, by = "SampleCode") %>% 
  left_join(Perim2Area, by = "SampleCode") %>%
  filter(!is.na(Width)) %>% 
  filter(!is.na(Perim2Area)) %>% # no catchment
  left_join(SamplingYear, "StudyCode") %>% 
  dplyr::select(MeanOpenCano, Width, MeanPop, os_18,os_20, os_21, os_22, os_23, OsClassB100, PopB100, OpenCanoB100, Perim2Area, Meandering, FeowId,SamplingYear, MeanDiet)

# MixOmics doesn't want categorical varibles in a dataframe -> convert into dummies
Osb100Carab<-dummies::dummy(DataPlsCarab$OsClassB100, sep = "_")
FeowidCarab<-dummies::dummy(DataPlsCarab$FeowId, sep = "_")
DataPlsCarab<-as.data.frame(cbind(DataPlsCarab, Osb100Carab, FeowidCarab))
```

## Run PLS on carabid dataset
```{r}
PlsCarab<-mixOmics::pls(DataPlsCarab[,c(1:8,10:13,15,17:22)],DataPlsCarab[,16], mode = "regression", scale = TRUE, ncomp = 10)

# Choose number of components
TunePlsCarab <- perf(PlsCarab, validation = "Mfold", folds = 10, progressBar = FALSE, nrepeat = 10)   
plot(TunePlsCarab$Q2.total) #-> 2 comp
TunePlsCarab$Q2.total

# Run PLS with 2 components
PlsCarab2C<-mixOmics::pls(DataPlsCarab[,c(1:8,10:13,15,17:22)],DataPlsCarab[,16], mode = "regression", scale = TRUE, ncomp = 2)

TunePlsCarab2C <- perf(PlsCarab2C, validation = "Mfold", folds = 10, progressBar = FALSE, nrepeat = 10)   
TunePlsCarab2C$R2
PlsCarab$mat.c

# Select explicative variables according to vip values
PlsCarab2C_vip<-vip(PlsCarab2C)
PlsCarab2C_vip

# Plot loadings values
plotLoadings(PlsCarab2C, block = 1, comp = 1, xlim = c(-0.4,0.4), border = TRUE)
```

# Partial least square regressions, removing some variables (reviewer 2)

## Run PLS on spider dataset with less local var

```{r}
PlsSpid_r1<-mixOmics::pls(DataPlsSpid[,c(1:8,12,13,15,20:31)], DataPlsSpid[,16], mode = "regression", scale = TRUE, ncomp = 10)

# Choose number of components
TunePlsSpid_r1 <- perf(PlsSpid_r1, validation = "Mfold", folds = 10, progressBar = FALSE, nrepeat = 10)  
TunePlsSpid_r1$Q2.total
plot(TunePlsSpid_r1$Q2.total) #-> 2 components

# Run PLS with 2 components
PlsSpid2C_r1<-mixOmics::pls(DataPlsSpid[,c(1:8,12,13,15,20:31)],DataPlsSpid[,16], mode = "regression", scale = TRUE, ncomp = 2)

TunePlsSpid2C_r1 <- perf(PlsSpid2C, validation = "Mfold", folds = 10, progressBar = FALSE, nrepeat = 10)   
TunePlsSpid2C_r1$R2
PlsSpid2C_r1$loadings
PlsSpid2C_r1$mat.c

# Select explicative variables according to vip values

PlsSpid2C_r1_Vip<-vip(PlsSpid2C_r1)
PlsSpid2C_r1_Vip

# Plot loadings values
plotLoadings(PlsSpid2C_r1, block = 1, comp = , xlim = c(-0.3,0.3), border = TRUE)

```

## Run PLS on carabid dataset with less local var
```{r}
PlsCarab_r1<-mixOmics::pls(DataPlsCarab[,c(1:8,12,13,15,20:22)],DataPlsCarab[,16], mode = "regression", scale = TRUE, ncomp = 10)

# Choose number of components
TunePlsCarab_r1 <- perf(PlsCarab_r1, validation = "Mfold", folds = 10, progressBar = FALSE, nrepeat = 10)   
plot(TunePlsCarab$Q2.total) #-> 1 comp
TunePlsCarab$Q2.total

# Run PLS with 1 components
PlsCarab1C_r1<-mixOmics::pls(DataPlsCarab[,c(1:8,12,13,15,20:22)],DataPlsCarab[,16], mode = "regression", scale = TRUE, ncomp = 1)

TunePlsCarab1C <- perf(PlsCarab1C_r1, validation = "Mfold", folds = 10, progressBar = FALSE, nrepeat = 10)   
TunePlsCarab1C$R2
TunePlsCarab1C$mat.c
PlsCarab1C_r1$explained_variance


# Select explicative variables according to vip values
PlsCarab1C_vip<-vip(PlsCarab1C_r1)
PlsCarabC_vip

# Plot loadings values
plotLoadings(PlsCarab2C, block = 1, comp = 1, xlim = c(-0.4,0.4), border = TRUE)
```